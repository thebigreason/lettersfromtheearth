<?php
	
	// Set up chapter array
	$toc = array(
		
		// 1 	=> array('slug' => 'preface', 'title' => 'Preface by Henry Nash Smith'), 
		1 	=> array('slug' => 'introduction', 'title' => 'Introduction'),
		2 	=> array('slug' => 'satans-letter', 'title' => 'Satan&rsquo;s Letter'),
		3 	=> array('slug' => 'letter-ii', 'title' => 'Letter II'),
		4 	=> array('slug' => 'letter-iii', 'title' => 'Letter III'),
		5 	=> array('slug' => 'letter-iv', 'title' => 'Letter IV'),
		6 	=> array('slug' => 'letter-v', 'title' => 'Letter V'),
		7 	=> array('slug' => 'letter-vi', 'title' => 'Letter VI'),
		8 	=> array('slug' => 'letter-vii', 'title' => 'Letter VII'),
		9 	=> array('slug' => 'letter-viii', 'title' => 'Letter VIII'),
		10 	=> array('slug' => 'letter-ix', 'title' => 'Letter IX'),
		11 	=> array('slug' => 'letter-x', 'title' => 'Letter X'),
		12 	=> array('slug' => 'letter-xi', 'title' => 'Letter XI'),
		13 	=> array('slug' => 'notes', 'title' => 'Publisher&rsquo;s Notes')
	
	);
	
	// Set up page array
	$page = array(
		
		'current' 		=> NULL, 
		'index' 		=> 0, 
		'slug' 			=> 'index', 
		'chapter'		=> NULL, 
		'prev-slug'		=> NULL, 
		'next-slug' 	=> NULL, 
		'prev-chapter'	=> NULL, 
		'next-chapter'	=> NULL
	
	);
	
	function getCurrentChapterIndex ($array, $attr, $val, $strict = FALSE) {
		
		// Error if input is not an array
		if (!is_array($array)) return FALSE;
		
		// Loop through the array
		foreach ($array as $key => $inner) {
		
			// Error if inner item is not an array
			if (!is_array($inner)) return FALSE;
			
			// Skip entries where search key is not present
			if (!isset($inner[$attr])) continue;
			
			if ($strict) {
			
				// Strict typing
				if ($inner[$attr] === $val) return $key;
			
			} else {
			
				// Loose typing
				if ($inner[$attr] == $val) return $key;
			
			}
			
		}
		
		// Slug not found
		return NULL;
	}
	
	// Get current page
	if (!empty($_GET['slug'])) {

		$page['current'] = $_GET['slug'];
		$page['index'] = getCurrentChapterIndex($toc, 'slug', $page['current']);
		$page['slug'] = $toc[$page['index']]['slug'];
		$page['chapter'] = $toc[$page['index']]['title'];

	}
	
	// Inc and dec the page indexes for navigation
	$prev = $page['index']-1;
	$next = $page['index']+1;
	
	// Get previous and next page for navigation
	if ($prev > 0) {
		$page['prev-slug'] = $toc[$prev]['slug'];
		$page['prev-chapter'] = $toc[$prev]['title'];
	}
	if ($next < 15) {
		$page['next-slug'] = $toc[$next]['slug'];
		$page['next-chapter'] = $toc[$next]['title'];
	}
	
	// Set current page title
	if ($page['slug'] == 'index') {
		$page['title'] = $page['chapter'] . 'Letters From The Earth';
	} else {
		$page['title'] = $page['chapter'] . ' | Letters From The Earth';
	}
	
	// Set the page class to the current page slug
	$page['class'] = $page['slug'];
