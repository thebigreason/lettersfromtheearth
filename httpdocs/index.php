<? include('../engine/Bootstrap.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?=$page['title']?></title>
        <meta name="description" content="Letters from the Earth by Mark Twain" />
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="/css/normalize.min.css">
        <link rel="stylesheet" href="/css/main.min.css" />
    </head>
    <body class="<?=$page['class']?>">
        <div class="main">
			<? include('inc/' . $page['slug'] . '.inc.php'); ?>
        </div>

        <footer>
			<? include('inc/navigation.inc.php'); ?>
			<small class="meta">Title: <?=$page['chapter']?> | Slug: <?=$page['slug']?> | Index: <?=$page['index']?></small>
		</footer>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-74039-11', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>
</html>
