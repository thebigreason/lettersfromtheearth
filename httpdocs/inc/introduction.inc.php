<h1>Introduction</h1>

<p>The Creator sat upon the throne, thinking. Behind him stretched the illimitable continent of heaven, steeped in a glory of light and color; before him rose the black night of Space, like a wall. His mighty bulk towered rugged and mountain-like into the zenith, and His divine head blazed there like a distant sun. At His feet stood three colossal figures, diminished to extinction, almost, by contrast&mdash;archangels&mdash;their heads level with His ankle-bone.</p>

<p>When the Creator had finished thinking, He said, &ldquo;I have thought. Behold!&rdquo;</p>

<p>He lifted His hand, and from it burst a fountain-spray of fire, a million stupendous suns, which clove the blackness and soared, away and away and away, diminishing in magnitude and intensity as they pierced the far frontiers of Space, until at last they were but as diamond nailheads sparkling under the domed vast roof of the universe.</p>

<p>At the end of an hour the Grand Council was dismissed.</p>

<p>They left the Presence impressed and thoughtful, and retired to a private place, where they might talk with freedom. None of the three seemed to want to begin, though all wanted somebody to do it. Each was burning to discuss the great event, but would prefer not to commit himself till he should know how the others regarded it. So there was some aimless and halting conversation about matters of no consequence, and this dragged tediously along, arriving nowhere, until at last the archangel Satan gathered his courage together&mdash;of which he had a very good supply&mdash;and broke ground. He said: &ldquo;We know what we are here to talk about, my lords, and we may as well put pretense aside, and begin. If this is the opinion of the Council&mdash;&rdquo;</p>

<p>&ldquo;It is, it is!&rdquo; said Gabriel and Michael, gratefully interrupting.</p>

<p>&ldquo;Very well, then, let us proceed. We have witnessed a wonderful thing; as to that, we are necessarily agreed. As to the value of it&mdash;if it has any&mdash;that is a matter which does not personally concern us. We can have as many opinions about it as we like, and that is our limit. We have no vote. I think Space was well enough, just as it was, and useful, too. Cold and dark&mdash;a restful place, now and then, after a season of the overdelicate climate and trying splendors of heaven. But these are details of no considerable moment; the new feature, the immense feature, is&mdash;what, gentlemen?&rdquo;</p>

<p>&ldquo;The invention and introduction of automatic, unsupervised, self-regulating <em>law</em> for the government of those myriads of whirling and racing suns and worlds!&rdquo;</p>

<p>&ldquo;That is it!&rdquo; said Satan. &ldquo;You perceive that it is a stupendous idea. Nothing approaching it has been evolved from the Master Intellect before. Law&mdash;<em>Automatic</em> Law&mdash;exact and unvarying Law&mdash;requiring no watching, no correcting, no readjusting while the eternities endure! He said those countless vast bodies would plunge through the wastes of Space ages and ages, at unimaginable speed, around stupendous orbits, yet never collide, and never lengthen nor shorten their orbital periods by so much as the hundredth part of a second in two thousand years! That is the new miracle, and the greatest of all&mdash;<em>Automatic Law!</em> And He gave it a name&mdash;the LAW OF NATURE&mdash;and said Natural Law is the LAW OF GOD&mdash;interchangeable names for one and the same thing.&rdquo;</p>

<p>&ldquo;Yes,&rdquo; said Michael, &ldquo;and He said He would establish Natural Law&mdash;the Law of God&mdash;throughout His dominions, and its authority should be supreme and inviolable.&rdquo;</p>

<p>&ldquo;Also,&rdquo; said Gabriel, &ldquo;He said He would by and by create animals, and place them, likewise, under the authority of that Law.&rdquo;</p>

<p>&ldquo;Yes,&rdquo; said Satan, &ldquo;I heard Him, but did not understand. What <em>is</em> animals, Gabriel?&rdquo;</p>

<p>&ldquo;Ah, how should I know? How should any of us know? It is a new word.&rdquo;</p>

<p>[<em>Interval of three centuries, celestial time&mdash;the equivalent of a hundred million years, earthly time. Enter a Messenger-Angel.</em>]</p>

<p>&ldquo;My lords, He is making animals. Will it please you to come and see?&rdquo;</p>

<p>They went, they saw, and were perplexed. Deeply perplexed&mdash;and the Creator noticed it, and said, &ldquo;Ask. I will answer.&rdquo;</p>

<p>&ldquo;Divine One,&rdquo; said Satan, making obeisance, &ldquo;what are they for?&rdquo;</p>

<p>&ldquo;They are an experiment in Morals and Conduct. Observe them, and be instructed.&rdquo;</p>

<p>There were thousands of them. They were full of activities. Busy, all busy&mdash;mainly in persecuting each other. Satan remarked&mdash;after examining one of them through a powerful microscope: &ldquo;This large beast is killing weaker animals, Divine One.&rdquo;</p>

<p>&ldquo;The tiger&mdash;yes. The law of his nature is ferocity. The law of his nature is the Law of God. He cannot disobey it.&rdquo;</p>

<p>&ldquo;Then in obeying it he commits no offense, Divine One?&rdquo;</p>

<p>&ldquo;No, he is blameless.&rdquo;</p>

<p>&ldquo;This other creature, here, is timid, Divine One, and suffers death without resisting.&rdquo;</p>

<p>&ldquo;The rabbit&mdash;yes. He is without courage. It is the law of his nature&mdash;the Law of God. He must obey it.&rdquo;</p>

<p>&ldquo;Then he cannot honorably be required to go counter to his nature and resist, Divine One?&rdquo;</p>

<p>&ldquo;No. No creature can be honorably required to go counter to the law of his nature&mdash;the Law of God.&rdquo;</p>

<p>After a long time and many questions, Satan said, &ldquo;The spider kills the fly, and eats it; the bird kills the spider and eats it; the wildcat kills the goose; the&mdash;well, they all kill each other. It is murder all along the line. Here are countless multitudes of creatures, and they all kill, kill, kill, they are all murderers. And they are not to blame, Divine One?&rdquo;</p>

<p>&ldquo;They are not to blame. It is the law of their nature. And always the law of nature is the Law of God. Now&mdash;observe&mdash;behold! A new creature&mdash;and the masterpiece&mdash;<em>Man!</em>&rdquo;</p>

<p>Men, women, children, they came swarming in flocks, in droves, in millions.</p>

<p>&ldquo;What shall you do with them, Divine One?&rdquo;</p>

<p>&ldquo;Put into each individual, in differing shades and degrees, all the various Moral Qualities, in mass, that have been distributed, a single distinguishing characteristic at a time, among the nonspeaking animal world&mdash;courage, cowardice, ferocity, gentleness, fairness, justice, cunning, treachery, magnanimity, cruelty, malice, malignity, lust, mercy, pity, purity, selfishness, sweetness, honor, love, hate, baseness, nobility, loyalty, falsity, veracity, untruthfulness&mdash;each human being shall have <em>all</em> of these in him, and they will constitute his nature. In some, there will be high and fine characteristics which will submerge the evil ones, and those will be called good men; in others the evil characteristics will have dominion, and those will be called bad men. Observe&mdash;behold&mdash;they vanish!&rdquo;</p>

<p>&ldquo;Whither are they gone, Divine One?&rdquo;</p>

<p>&ldquo;To the earth&mdash;they and all their fellow animals.&rdquo;</p>

<p>&ldquo;What is the earth?&rdquo;</p>

<p>&ldquo;A small globe I made, a time, two times and a half ago. You saw it, but did not notice it in the explosion of worlds and suns that sprayed from my hand. Man is an experiment, the other animals are another experiment. Time will show whether they were worth the trouble. The exhibition is over; you may take your leave, my lords.&rdquo;</p>

<p>Several days passed by.</p>

<p>This stands for a long stretch of (our) time, since in heaven a day is as a thousand years.</p>

<p>Satan had been making admiring remarks about certain of the Creator&rsquo;s sparkling industries&mdash;remarks which, being read between the lines, were sarcasms. He had made them confidentially to his safe friends the other archangels, but they had been overheard by some ordinary angels and reported at Headquarters.</p>

<p>He was ordered into banishment for a day&mdash;the celestial day. It was a punishment he was used to, on account of his too flexible tongue. Formerly he had been deported into Space, there being nowhither else to send him, and had flapped tediously around there in the eternal night and the Arctic chill; but now it occurred to him to push on and hunt up the earth and see how the Human-Race experiment was coming along.</p>

<p>By and by he wrote home&mdash;very privately&mdash;to St. Michael and St. Gabriel about it.</p>
