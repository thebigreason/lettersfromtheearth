<h1>Letter XI</h1>

<p>Human history in all ages is red with blood, and bitter with hate, and stained with cruelties; but not since Biblical times have these features been without a limit of some kind. Even the Church, which is credited with having spilt more innocent blood, since the beginning of its supremacy, than all the political wars put together have spilt, has observed a limit. A sort of limit. But you notice that when the Lord God of Heaven and Earth, adored Father of Man, goes to war, there is no limit. He is totally without mercy&mdash;he, who is called the Fountain of Mercy. He slays, slays, slays! All the men, all the beasts, all the boys, all the babies; also all the women and all the girls, except those that have not been&nbsp;deflowered.</p>

<p>He makes no distinction between innocent and guilty. The babies were innocent, the beasts were innocent, many of the men, many of the women, many of the boys, many of the girls were innocent, yet they had to suffer with the guilty. What the insane Father required was blood and misery; he was indifferent as to who furnished&nbsp;it.</p>

<p>The heaviest punishment of all was meted out to persons who could not by any possibility have deserved so horrible a fate&mdash;the 32,000 virgins. Their naked privacies were probed, to make sure that they still possessed the hymen unruptured; after this humiliation they were sent away from the land that had been their home, to be sold into slavery; the worst of slaveries and the shamefulest, the slavery of prostitution; bed-slavery, to excite lust, and satisfy it with their bodies; slavery to any buyer, be he gentleman or be he a coarse and filthy&nbsp;ruffian.</p>

<p>It was the Father that inflicted this ferocious and undeserved punishment upon those bereaved and friendless virgins, whose parents and kindred he had slaughtered before their eyes. And were they praying to him for pity and rescue, meantime? Without a doubt of&nbsp;it.</p>

<p>These virgins were &ldquo;spoil&rdquo; plunder, booty. He claimed his share and got it. What use had <em>he</em> for virgins? Examine his later history and you will&nbsp;know.</p>

<p>His priests got a share of the virgins, too. What use could priests make of virgins? The private history of the Roman Catholic confessional can answer that question for you. The confessional&rsquo;s chief amusement has been seduction&mdash;in all the ages of the Church. P&egrave;re Hyacinth testifies that of a hundred priests confessed by him, ninety-nine had used the confessional effectively for the seduction of married women and young girls. One priest confessed that of nine hundred girls and women whom he had served as father and confessor in his time, none had escaped his lecherous embrace but the elderly and the homely. The official list of questions which the priest is required to ask will overmasteringly excite any woman who is not a&nbsp;paralytic.</p>

<p>There is nothing in either savage or civilized history that is more utterly complete, more remorselessly sweeping than the Father of Mercy&rsquo;s campaign among the Midianites. The official report does not furnish the incidents, episodes, and minor details, it deals only in information in masses: <em>all</em> the virgins, <em>all</em> the men, <em>all</em> the babies, <em>all</em> &ldquo;creatures <em>that breathe</em>,&rdquo; <em>all</em> houses, <em>all</em> cities; it gives you just one vast picture, spread abroad here and there and yonder, as far as eye can reach, of charred ruin and storm-swept desolation; your imagination adds a brooding stillness, an awful hush&mdash;the hush of death. But of course there were incidents. Where shall we get&nbsp;them?</p>

<p>Out of history of yesterday&rsquo;s date. Out of history made by the red Indian of America. He has duplicated God&rsquo;s work, and done it in the very spirit of God. In 1862 the Indians in Minnesota, having been deeply wronged and treacherously treated by the government of the United States, rose against the white settlers and massacred them; massacred all they could lay their hands upon, sparing neither age nor sex. Consider this&nbsp;incident:</p>

<p>Twelve Indians broke into a farmhouse at daybreak and captured the family. It consisted of the farmer and his wife and four daughters, the youngest aged fourteen and the eldest eighteen. They crucified the parents; that is to say, they stood them stark naked against the wall of the living room and nailed their hands to the wall. Then they stripped the daughters bare, stretched them upon the floor in front of their parents, and repeatedly ravished them. Finally they crucified the girls against the wall opposite this parents, and cut off their noses and their breasts. They also&mdash;but I will not go into that. There is a limit. There are indignities so atrocious that the pen cannot write them. One member of that poor crucified family&mdash;the father&mdash;was still alive when help came two days&nbsp;later.</p>

<p>Now you have one incident of the Minnesota massacre. I could give you fifty. They would cover all the different kinds of cruelty the brutal human talent has ever&nbsp;invented.</p>

<p>And now you know, by these sure indications, what happened under the personal direction of the Father of Mercies in his Midianite campaign. The Minnesota campaign was merely a duplicate of the Midianite raid. Nothing happened in the one that didn&rsquo;t happen in the&nbsp;other.</p>

<p>No, that is not strictly true. The Indian was more merciful than was the Father of Mercies. He sold no virgins into slavery to minister to the lusts of the murderers of their kindred while their sad lives might last; he raped them, then charitably made their subsequent sufferings brief, ending them with the precious gift of death. He burned some of the houses, but not all of them. He carried out innocent dumb brutes, but he took the lives of&nbsp;none.</p>

<p>Would you expect this same conscienceless God, this moral bankrupt, to become a teacher of morals; of gentleness; of meekness; of righteousness; of purity? It looks impossible, extravagant; but listen to him. These are his own&nbsp;words:</p>

<blockquote>
	<p>Blessed are the poor in spirit, for theirs is the kingdom of&nbsp;heaven.</p>
	<p>Blessed are they that mourn, for they shall be&nbsp;comforted.</p>
	<p>Blessed are the meek, for they shall inherit the&nbsp;earth.</p>
	<p>Blessed are they which do hunger and thirst after righteousness, for they shall be&nbsp;filled.</p>
	<p><em>Blessed are the merciful</em>, for they shall obtain&nbsp;mercy.</p>
	<p>Blessed are the pure in heart, for they shall see&nbsp;God.</p>
	<p><em>Blessed are the peacemakers</em>, for they shall be called <em>the children of&nbsp;God</em>.</p>
	<p>Blessed are they which are persecuted for righteousness&rsquo; sake, for theirs is the kingdom of&nbsp;heaven.</p>
	<p>Blessed are ye, when men shall revile you, and persecute you, and say all manner of evil against you falsely, for my&nbsp;sake.</p>
</blockquote>

<p>The mouth that uttered these immense sarcasms, these giant hypocrisies, is the very same that ordered the wholesale massacre of the Midianitish men and babies and cattle; the wholesale destruction of house and city; the wholesale banishment of the virgins into a filthy and unspeakable slavery. This is the same person who brought upon the Midianites the fiendish cruelties which were repeated by the red Indians, detail by detail, in Minnesota eighteen centuries later. The Midianite episode filled him with joy. So did the Minnesota one, or he would have prevented&nbsp;it.</p>

<p>The Beatitudes and the quoted chapters from Numbers and Deuteronomy ought always to be read from the pulpit together; then the congregation would get an all-round view of Our Father in Heaven. Yet not in a single instance have I ever known a clergyman to do&nbsp;this.</p>
