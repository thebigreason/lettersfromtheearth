<nav id="nav">
	<ul>
		<? if ($page['prev-slug']) { ?>
		<li>
			<a href="/<?=$page['prev-slug']?>/">&larr;<?=$page['prev-chapter']?></a>
		</li>
		<? } ?>
		<li><a href="./#top">Top</a></li>
		<li><a href="/">Index</a></li>
		<? if ($page['next-slug']) { ?>
		<li>
			<a href="/<?=$page['next-slug']?>/"><?=$page['next-chapter']?>&rarr;</a>
		</li>
		<? } ?>
	</ul>
</nav>
