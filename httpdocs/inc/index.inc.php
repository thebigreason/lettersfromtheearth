<header>
	<h1>Letters From The Earth</h1>
	<h2>by Mark Twain</h2>
	<h3>Edited by Bernard DeVoto</h3>
</header>

<nav id="toc">
	<ol>
		<? foreach ($toc as $chapter) { ?>
		
		<li><a href="/<?=$chapter['slug']?>/"><?=$chapter['title']?></a></li> 
		
		<? } ?>
	</ol>
</nav>
