<h1>Letter II</h1>

<p>&ldquo;I have told you nothing about man that is not true.&rdquo; You must pardon me if I repeat that remark now and then in these letters; I want you to take seriously the things I am telling you, and I feel that if I were in your place and you in mine, I should need that reminder from time to time, to keep my credulity from&nbsp;flagging.</p>

<p>For there is nothing about man that is not strange to an immortal. He looks at nothing as we look at it, his sense of proportion is quite different from ours, and his sense of values is so widely divergent from ours, that with all our large intellectual powers it is not likely that even the most gifted among us would ever be quite able to understand&nbsp;it.</p>

<p>For instance, take this sample: he has imagined a heaven, and has left entirely out of it the supremest of all his delights, the one ecstasy that stands first and foremost in the heart of every individual of his race&mdash;and of ours&mdash;sexual&nbsp;intercourse!</p>

<p>It is as if a lost and perishing person in a roasting desert should be told by a rescuer he might choose and have all longed-for things but one, and he should elect to leave out&nbsp;water!</p>

<p>His heaven is like himself: strange, interesting, astonishing, grotesque. I give you my word, it has not a single feature in it that he <em>actually values</em>. It consists&mdash;utterly and entirely&mdash;of diversions which he cares next to nothing about, here in the earth, yet is quite sure he will like them in heaven. Isn&rsquo;t it curious? Isn&rsquo;t it interesting? You must not think I am exaggerating, for it is not so. I will give you&nbsp;details.</p>

<p>Most men do not sing, most men cannot sing, most men will not stay when others are singing if it be continued more than two hours. Note&nbsp;that.</p>

<p>Only about two men in a hundred can play upon a musical instrument, and not four in a hundred have any wish to learn how. Set that&nbsp;down.</p>

<p>Many men pray, not many of them like to do it. A few pray long, the others make a short&nbsp;cut.</p>

<p>More men go to church than want&nbsp;to.</p>

<p>To forty-nine men in fifty the Sabbath Day is a dreary, dreary&nbsp;bore.</p>

<p>Of all the men in a church on a Sunday, two-thirds are tired when the service is half over, and the rest before it is&nbsp;finished.</p>

<p>The gladdest moment for all of them is when the preacher uplifts his hands for the benediction. You can hear the soft rustle of relief that sweeps the house, and you recognize that it is eloquent with&nbsp;gratitude.</p>

<p>All nations look down upon all other&nbsp;nations.</p>

<p>All nations dislike all other&nbsp;nations.</p>

<p>All white nations despise all colored nations, of whatever hue, and oppress them when they&nbsp;can.</p>

<p>White men will not associate with &ldquo;niggers,&rdquo; nor marry&nbsp;them.</p>

<p>They will not allow them in their schools and&nbsp;churches.</p>

<p>All the world hates the Jew, and will not endure him except when he is&nbsp;rich.</p>

<p>I ask you to note all those&nbsp;particulars.</p>

<p>Further. All sane people detest&nbsp;noise.</p>

<p>All people, sane or insane, like to have variety in their life. Monotony quickly wearies&nbsp;them.</p>

<p>Every man, according to the mental equipment that has fallen to his share, exercises his intellect constantly, ceaselessly, and this exercise makes up a vast and valued and essential part of his life. The lowest intellect, like the highest, possesses a skill of some kind and takes a keen pleasure in testing it, proving it, perfecting it. The urchin who is his comrade&rsquo;s superior in games is as diligent and as enthusiastic in his practice as are the sculptor, the painter, the pianist, the mathematician and the rest. Not one of them could be happy if his talent were put under an&nbsp;interdict.</p>

<p>Now then, you have the facts. You know what the human race enjoys, and what it doesn&rsquo;t enjoy. It has invented a heaven out of its own head, all by itself: guess what it is like! In fifteen hundred eternities you couldn&rsquo;t do it. The ablest mind known to you or me in fifty million aeons couldn&rsquo;t do it. Very well, I will tell you about&nbsp;it.</p>

<p>1. First of all, I recall to your attention the extraordinary fact with which I began. To wit, that the human being, like the immortals, naturally places sexual intercourse far and away above all other joys&mdash;yet he has left it out of his heaven! The very thought of it excites him; opportunity sets him wild; in this state he will risk life, reputation, everything&mdash;even his queer heaven itself&mdash;to make good that opportunity and ride it to the overwhelming climax. From youth to middle age all men and all women prize copulation above all other pleasures combined, yet it is actually as I have said: it is not in their heaven; prayer takes its&nbsp;place.</p>

<p>They prize it thus highly; yet, like all their so-called &ldquo;boons,&rdquo; it is a poor thing. At its very best and longest the act is brief beyond imagination&mdash;the imagination of an immortal, I mean. In the matter of repetition the man is limited&mdash;oh, quite beyond immortal conception. We who continue the act and its supremest ecstasies unbroken and without withdrawal for centuries, will never be able to understand or adequately pity the awful poverty of these people in that rich gift which, possessed as we possess it, makes all other possessions trivial and not worth the trouble of&nbsp;invoicing.</p>

<p>2. In man&rsquo;s heaven <em>everybody sings!</em> The man who did not sing on earth sings there; the man who could not sing on earth is able to do it there. The universal singing is not casual, not occasional, not relieved by intervals of quiet; it goes on, all day long, and every day, during a stretch of twelve hours. And <em>everybody stays;</em> whereas in the earth the place would be empty in two hours. The singing is of hymns alone. Nay, it is of <em>one</em> hymn alone. The words are always the same, in number they are only about a dozen, there is no rhyme, there is no poetry: &ldquo;Hosannah, hosannah, hosannah, Lord God of Sabaoth, &rsquo;rah! &rsquo;rah! &rsquo;rah! siss!&mdash;boom!&nbsp;&hellip;&nbsp;a-a-ah!&rdquo;</p>

<p>3. Meantime, every person is playing on a harp&mdash;those millions and millions!&mdash;whereas not more than twenty in the thousand of them could play an instrument in the earth, or ever wanted&nbsp;to.</p>

<p>Consider the deafening hurricane of sound&mdash;millions and millions of voices screaming at once and millions and millions of harps gritting their teeth at the same time! I ask you: is it hideous, is it odious, is it&nbsp;horrible?</p>

<p>Consider further: it is a <em>praise</em> service; a service of compliment, of flattery, of adulation! Do you ask who it is that is willing to endure this strange compliment, this insane compliment; and who not only endures it, but likes it, enjoys it, requires if, <em>commands</em> it? Hold your&nbsp;breath!</p>

<p>It is God! This race&rsquo;s god, I mean. He sits on his throne, attended by his four and twenty elders and some other dignitaries pertaining to his court, and looks out over his miles and miles of tempestuous worshipers, and smiles, and purrs, and nods his satisfaction northward, eastward, southward; as quaint and na&iuml;ve a spectacle as has yet been imagined in this universe, I take&nbsp;it.</p>

<p>It is easy to see that the inventor of the heavens did not originate the idea, but copied it from the show-ceremonies of some sorry little sovereign State up in the back settlements of the Orient&nbsp;somewhere.</p>

<p>All sane white people hate noise; yet they have tranquilly accepted this kind of heaven&mdash;without thinking, without reflection, without examination&mdash;and they actually want to go to it! Profoundly devout old gray-headed men put in a large part of their time dreaming of the happy day when they will lay down the cares of this life and enter into the joys of that place. Yet you can see how unreal it is to them, and how little it takes a grip upon them as being fact, for they make no practical preparation for the great change: you never see one of them with a harp, you never hear one of them&nbsp;sing.</p>

<p>As you have seen, that singular show is a service of praise: praise by hymn, praise by prostration. It takes the place of &ldquo;church.&rdquo; Now then, in the earth these people cannot stand much church&mdash;an hour and a quarter is the limit, and they draw the line at once a week. That is to say, Sunday. One day in seven; and even then they do not look forward to it with longing. And so&mdash;consider what their heaven provides for them: &ldquo;church&rdquo; that lasts forever, and a Sabbath that has no end! They quickly weary of this brief hebdomadal Sabbath here, yet they long for that eternal one; they dream of it, they talk about it, they <em>think</em> they think they are going to enjoy it&mdash;with all their simple hearts they think they think they are going to be happy in&nbsp;it!</p>

<p>It is because they do not think at all; they only think they think. Whereas they can&rsquo;t think; not two human beings in ten thousand have anything to think with. And as to imagination&mdash;oh, well, look at their heaven! They accept it, they approve it, they admire it. That gives you their intellectual&nbsp;measure.</p>

<p>4. The inventor of their heaven empties into it all the nations of the earth, in one common jumble. All are on an equality absolute, no one of them ranking another; they have to be &ldquo;brothers&rdquo;; they have to mix together, pray together, harp together, hosannah together&mdash;whites, niggers, Jews, everybody&mdash;there&rsquo;s no distinction. Here in the earth all nations hate each other, and every one of them hates the Jew. Yet every pious person adores that heaven and wants to get into it. He really does. And when he is in a holy rapture he thinks he thinks that if he were only there he would take all the populace to his heart, and hug, and hug, and&nbsp;hug!</p>

<p>He is a marvel&mdash;man is! I would I knew who invented&nbsp;him.</p>

<p>5. Every man in the earth possesses some share of intellect, large or small; and be it large or be it small he takes pride in it. Also his heart swells at mention of the names of the majestic intellectual chiefs of his race, and he loves the tale of their splendid achievements. For he is of their blood, and in honoring themselves they have honored him. Lo, what the mind of man can do! he cries; and calls the roll of the illustrious of all ages; and points to the imperishable literatures they have given to the world, and the mechanical wonders they have invented, and the glories wherewith they have clothed science and the arts; and to them he uncovers as to kings, and gives to them the profoundest homage, and the sincerest, his exultant heart can furnish&mdash;thus exalting intellect above all things else in the world, and enthroning it there under the arching skies in a supremacy unapproachable. And then he contrived a heaven that hasn&rsquo;t a rag of intellectuality in it&nbsp;anywhere!</p>

<p>Is it odd, is it curious, is it puzzling? It is exactly as I have said, incredible as it may sound. This sincere adorer of intellect and prodigal rewarder of its mighty services here in the earth has invented a religion and a heaven which pay no compliments to intellect, offer it no distinctions, fling it no largess: in fact, never even mention&nbsp;it.</p>

<p>By this time you will have noticed that the human being&rsquo;s heaven has been thought out and constructed upon an absolute definite plan; and that this plan is, that it shall contain, in labored detail, each and every imaginable thing that is repulsive to a man, and not a single thing he&nbsp;likes!</p>

<p>Very well, the further we proceed the more will this curious fact be&nbsp;apparent.</p>

<p>Make a note of it: in man&rsquo;s heaven there are no exercises for the intellect, nothing for it to live upon. It would rot there in a year&mdash;rot and stink. Rot and stink&mdash;and at that stage become holy. A blessed thing: for only the holy can stand the joys of that&nbsp;bedlam.</p>
