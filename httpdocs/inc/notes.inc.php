<h1>Publisher&rsquo;s Notes</h1>

<p>I first became aware of this book in 2013, and while looking for a suitable e-book version to purchase, I saw that it was available for free on the <a href="http://www.sacred-texts.com/">Sacred Texts Archive</a>&nbsp;website.</p>

<p>I began reading it there, but quickly became annoyed by typos and the poor typography of the web page. I continued my search for a well-designed e-book edition, but every edition of the e-book I could find was simply a copy of the same <a href="http://www.sacred-texts.com/aor/twain/letearth.htm">Sacred Text Archive version</a>; Each one contained the same typos and poor typography (double hyphens for em dashes, strait quotes and apostrophes,&nbsp;etc.).</p>

<p>I arbitrarily chose an inexpensive copy of the e-book, and despite the poor editing, I was floored by the content of its pages. As a former Christian and someone very familiar with the writings of the <a href="//en.wikipedia.org/wiki/New_Atheism#Prominent_figures">four horsemen of the non-apocalypse</a>, I was delighted by the realization that Mark Twain would have easily been counted among the most &ldquo;strident&rdquo; of these &ldquo;new atheist&rdquo; authors, had this book been published after the events of September&nbsp;11,&nbsp;2001.</p>

<p>While I am an advocate for the e-book format and the possibilities it opens up for the convenience and portability of literature and information, I am a web developer at heart, which is why I chose to publish these pages here. I also have a passion for type. The dire state of e-book typography and the ignorance/laziness of the modern publishing industry infuriates me to no end. This is what compelled me to step in and provide an acceptable reading experience for this classic&nbsp;work.</p>

<p>I began the process by copying the text from the Sacred Texts Archive and replacing all of the punctuation with proper typographic characters. While reading the e-book, I took note of missing words, sentences beginning with lower case letters, and some of the more questionable punctuation choices. The next step was to correct these errors, but without a reference copy of the original book, I wasn&rsquo;t willing to take this liberty. Given the number of these errors, I couldn&rsquo;t rely on the Sacred Texts Archive verison to any&nbsp;degree.</p>

<p>I decided that the most credible reference would be a first edition copy of the book, and I quickly found one for sale on eBay for under $40 USD. The most striking difference between the first (print) edition and the e-books is that the first edition includes 13 additional essays: <em>Papers of the Adam Family</em>, <em>Letter to the Earth</em>, <em>A Cat-Tale</em>, <em>Cooper&rsquo;s Prose Style</em>, <em>Official Report to the I.I.A.S.</em>, <em>The Gorky Incident</em>, <em>Simplified Spelling</em>, <em>Something About Repentance</em>, <em>From An English Notebook</em>, <em>From the Manuscript of &ldquo;A Tramp Abroad&rdquo;: The French And The Comanches</em>, <em>From An Unfinished Burlesque of Books On Etiquette</em>, <em>The Damned Human Race</em>, and <em>The Great Dark</em>.</p>

<p>These works were written a few years prior to Samuel Clemens&rsquo; death in 1910, then compiled and edited for this volume by Bernard DeVoto in 1939, but were deemed too controversial to publish at the time. Many of them were not published prior to this book in 1962, and therefore are likely not considered public domain in the United States. I might like to pursue electronically publishing the other papers in this volume at some point, but as of now, <em>Letters from the&nbsp;Earth</em> appears to have the fewest legal barriers.</p>

<p>Upon receiving my first edition, I crossed referenced the errors from my notes and made the necessary corrections. Indeed there were missing words, capitalization errors, and punctuation that was not present in the first&nbsp;edition.</p>

<p>The next step was to find and apply the emphasis (bold and italics) from the original text. I did this by scanning the essay line by line, using a bookmark to mask each line as I went along. I am fairly certain that I was able to find everything, but there are some instances of italics on short words like &ldquo;he&rdquo; and &ldquo;I&rdquo; which are admittedly hard to&nbsp;find.</p>

<p>With the typos corrected and the proper entities in place, I was then able to decide how best to markup the more unique portions of the text, such as song verses, footnotes, block-quoted text, and excerpts of biblical&nbsp;text.</p>

<h2>Colophon</h2>

<p>In the spirit of the open web and its original concept as a publishing platform, this site is marked up with semantic HTML5 and lightly styled with CSS3. Google Analytics is used to record traffic to this publication, but no other JavaScript is present in these pages. I have chosen to break up each section of the essay into separate pages for easy bookmarking, and simplified maintanance and development by using PHP for templates, and generating the table of contents and navigation.</p>

<p>The essay is set in <a href="http://crudfactory.com/font/index">Fanwood</a> by <a href="http://crudfactory.com">Barry Schwartz</a>. The website was built with&nbsp;<a href="http://www.barebones.com/products/bbedit/">BBEdit</a>. Git is used for version control, and the repository is hosted on&nbsp;<a href="https://bitbucket.org/thebigreason/lettersfromtheearth">Bitbucket</a>.</p>

<section class="byline">
	<address>
		<h4>Mark Eagleton</h4>
		Publisher of this edition of <br />Letters from the&nbsp;Earth
	</address>
	<div>
		Woodland,&nbsp;CA<br />
		December,&nbsp;2014
	</div>
</section>
