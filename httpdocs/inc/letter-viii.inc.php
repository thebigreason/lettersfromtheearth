<h1>Letter VIII</h1>

<p>Man is without any doubt the most interesting fool there is. Also the most eccentric. He hasn&rsquo;t a single written law, in his Bible or out of it, which has any but just one purpose and intention&mdash;<em>to limit or defeat the law of&nbsp;God.</em></p>

<p>He can seldom take a plain fact and get any but a wrong meaning out of it. He cannot help this; it is the way the confusion he calls his mind is constructed. Consider the things he concedes, and the curious conclusions he draws from&nbsp;them.</p>

<p>For instance, he concedes that God made man. Made him without man&rsquo;s desire of&nbsp;privity.</p>

<p>This seems to plainly and indisputably make God, and God alone, responsible for man&rsquo;s acts. But man denies&nbsp;this.</p>

<p>He concedes that God has made the angels perfect, without blemish, and immune from pain and death, and that he could have been similarly kind to man if he had wanted to, but denies that he was under any moral obligation to do&nbsp;it.</p>

<p>He concedes that man has no moral right to visit the child of his begetting with wanton cruelties, painful diseases and death, but refuses to limit God&rsquo;s privileges in this sort with the children of his&nbsp;begetting.</p>

<p>The Bible and man&rsquo;s statutes forbid murder, adultery, fornication, lying, treachery, robbery, oppression and other crimes, but contend that God is free of these laws and has a right to break them when he&nbsp;will.</p>

<p>He concedes that God gives to each man his temperament, his disposition, at birth; he concedes that man cannot by any process change this temperament, but must remain always under its dominion. Yet if it be full of dreadful passions, in one man&rsquo;s case, and barren of them in another man&rsquo;s, it is right and rational to punish the one for his crimes, and reward the other for abstaining from&nbsp;crime.</p>

<p>There&mdash;let us consider these&nbsp;curiosities.</p>

<p><em>Temperament</em>&nbsp;(<em>Disposition</em>)</p>

<p>Take two extremes of temperament&mdash;the goat and the&nbsp;tortoise.</p>

<p>Neither of these creatures makes its own temperament, but is born with it, like man, and can no more change it than can&nbsp;man.</p>

<p>Temperament is the law of God written in the heart of every creature by God&rsquo;s own hand, and <em>must</em> be obeyed, and will be obeyed in spite of all restricting or forbidding statutes, let them emanate whence they&nbsp;may.</p>

<p>Very well, lust is the dominant feature of the goat&rsquo;s temperament, the law of God is in its heart, and it must obey it and <em>will</em> obey it the whole day long in the rutting season, without stopping to eat or drink. If the Bible said to the goat, &ldquo;Thou shalt not fornicate, thou shalt not commit adultery,&rdquo; even Man&mdash;sap-headed man&mdash;would recognize the foolishness of the prohibition, and would grant that the goat ought not to be punished for obeying the law of his Maker. Yet he thinks it right and just that man should be put under the prohibition. All men. All&nbsp;alike.</p>

<p>On its face this is stupid, for, by temperament, which is the <em>real</em> law of God, many men are goats and can&rsquo;t help committing adultery when they get a chance; whereas there are numbers of men who, by temperament, can keep their purity and let an opportunity go by if the woman lacks in attractiveness. But the Bible doesn&rsquo;t allow adultery at all, whether a person can help it or not. It allows no distinction between goat and tortoise&mdash;the excitable goat, the emotional goat, that has to have some adultery every day or fade and die; and the tortoise, that cold calm puritan, that takes a treat only once in two years and then goes to sleep in the midst of it and doesn&rsquo;t wake up for sixty days. No lady goat is safe from criminal assault, even on the Sabbath Day, when there is a gentleman goat within three miles to leeward of her and nothing in the way but a fence fourteen feet high, whereas neither the gentleman tortoise nor the lady tortoise is ever hungry enough for solemn joys of fornication to be willing to break the Sabbath to get them. Now according to man&rsquo;s curious reasoning, the goat has earned punishment, and the tortoise&nbsp;praise.</p>

<p>&ldquo;Thou shalt not commit adultery&rdquo; is a command which makes no distinction between the following persons. They are all required to obey it:</p>

<p>Children at birth.</p>

<p>Children in the cradle.</p>

<p>School children.</p>

<p>Youths and maidens.</p>

<p>Fresh adults.</p>

<p>Older ones.</p>

<p>Men and women of 40.</p>

<p>Of 50.</p>

<p>Of 60.</p>

<p>Of 70.</p>

<p>Of 80.</p>

<p>Of 90.</p>

<p>Of 100.</p>

<p>The command does not distribute its burden equally, and&nbsp;cannot.</p>

<p>It is not hard upon the three sets of&nbsp;children.</p>

<p>It is hard&mdash;harder&mdash;still harder upon the next three sets&mdash;cruelly&nbsp;hard.</p>

<p>It is blessedly softened to the next three&nbsp;sets.</p>

<p>It has now done all the damage it can, and might as well be put out of commission. Yet with comical imbecility it is continued, and the four remaining estates are put under its crushing ban. Poor old wrecks, they couldn&rsquo;t disobey if they tried. And think&mdash;because they holily refrain from adulterating each other, they get praise for it! Which is nonsense; for even the Bible knows enough to know that if the oldest veteran there could get his lost heyday back again for an hour he would cast that commandment to the winds and ruin the first woman he came across, even though she were an entire&nbsp;stranger.</p>

<p>It is as I have said: every statute in the Bible and in the law-books is an attempt to defeat a law of God&mdash;in other words an unalterable and indestructible law of nature. These people&rsquo;s God has shown them by a million acts that he respects none of the Bible&rsquo;s statutes. He breaks every one of them himself, adultery and&nbsp;all.</p>

<p>The law of God, as quite plainly expressed in woman&rsquo;s construction is this: There shall be no limit put upon your intercourse with the other sex sexually, at any time of&nbsp;life.</p>

<p>The law of God, as quite plainly expressed in man&rsquo;s construction is this: During your entire life you shall be under inflexible limits and restrictions,&nbsp;sexually.</p>

<p>During twenty-three days in every month (in absence of pregnancy) from the time a woman is seven years old till she dies of old age, she is ready for action, and <em>competent</em>. As competent as the candlestick is to receive the candle. Competent every day, competent every night. Also she <em>wants</em> that candle&mdash;yearns for it, longs for it, hankers after it, as commanded by the law of God in her&nbsp;heart.</p>

<p>But man is only briefly competent; and only then in the moderate measure applicable to the word in <em>his</em> sex&rsquo;s case. He is competent from the age of sixteen or seventeen thence-forward for thirty-five years. After fifty his performance is of poor quality, the intervals between are wide, and its satisfactions of no great value to either party; whereas his great-grandmother is as good as new. There is nothing the matter with her plant. Her candlestick is as firm as ever, whereas his candle is increasingly softened and weakened by the weather of age, as the years go by, until at last it can no longer stand, and is mournfully laid to rest in the hope of a blessed resurrection which is never to&nbsp;come.</p>

<p>By the woman&rsquo;s make, her plant has to be out of service three days in the month, and during a part of her pregnancy. These are times of discomfort, often of suffering. For fair and just compensation she has the high privilege of unlimited adultery all the other days of her&nbsp;life.</p>

<p>That is the law of God, as revealed in her make. What becomes of this high privilege? Does she live in free enjoyment of it? No. Nowhere in the whole world. She is robbed of it everywhere. Who does this? Man. Man&rsquo;s statutes&mdash;if the Bible <em>is</em> the Word of&nbsp;God.</p>

<p>Now there you have a sample of man&rsquo;s &ldquo;reasoning powers,&rdquo; as he calls them. He observes certain facts. For instance, that in all his life he never sees the day that he can satisfy one woman; also, that no woman ever sees the day that she can&rsquo;t overwork, and defeat, and put out of commission any ten masculine plants that can be put to bed to her.<a href="#fn-1" id="ref-1">*</a> He puts those strikingly suggestive and luminous facts together, and from them draws this astonishing conclusion: The Creator intended the woman to be restricted to one&nbsp;man.</p>

<p>So he concretes that singular conclusion into a <em>law</em>, for good and&nbsp;all.</p>

<p>And he does it without consulting the woman, although she has a thousand times more at stake in the matter than he has. His procreative competency is limited to an average of a hundred exercises per year for fifty years, hers is good for three thousand a year for that whole time&mdash;and as many years longer as she may live. Thus his life interest in the matter is five thousand refreshments, while hers is a hundred and fifty thousand; yet instead of fairly and honorably leaving the making of the law to the person who has an overwhelming interest at stake in it, this immeasurable hog, who has nothing at stake in it worth considering, makes it&nbsp;himself!</p>

<p>You have heretofore found out, by my teachings, that man is a fool; you are now aware that woman is a damned&nbsp;fool.</p>

<p>Now if you or any other really intelligent person were arranging the fairness and justices between man and woman, you would give the man one-fiftieth interest in one woman, and the woman a harem. Now wouldn&rsquo;t you? Necessarily. I give you my word, this creature with the decrepit candle has arranged it exactly the other way. Solomon, who was one of the Deity&rsquo;s favorites, had a copulation cabinet composed of seven hundred wives and three hundred concubines. To save his life he could not have kept two of these young creatures satisfactorily refreshed, even if he had had fifteen experts to help him. Necessarily almost the entire thousand had to go hungry years and years on a stretch. Conceive of a man hardhearted enough to look daily upon all that suffering and not be moved to mitigate it. He even wantonly added a sharp pang to that pathetic misery; for he kept within those women&rsquo;s sight, always, stalwart watchmen whose splendid masculine forms made the poor lassies&rsquo; mouths water but who hadn&rsquo;t anything to solace a candlestick with, these gentry being eunuchs. A eunuch is a person whose candle has been put out. By&nbsp;art.<a href="#fn-2" id="ref-2">**</a></p>

<p>From time to time, as I go along, I will take up a Biblical statute and show you that it always violates a law of God, and then is imported into the lawbooks of the nations, where it continues its violations. But those things will keep; there is no&nbsp;hurry.</p>

<footer>
	<article class="footnote" id="fn-1">
		<p>* In the Sandwich Islands in 1866 a buxom royal princess died. Occupying a place of distinguished honor at her funeral were thirty-six splendidly built young native men. In a laudatory song which celebrated the various merits, achievements and accomplishments of the late princess those thirty-six stallions were called her <em>harem</em>, and the song said it had been her pride and boast that she kept the whole of them busy, and that several times it had happened that more than one of them had been able to charge&nbsp;overtime. [M.T.] <a href="#ref-1">&#8617;</a></p>
	</article>
	<article class="footnote" id="fn-2">
		<p>** I purpose publishing these Letters here in the world before I return to you. Two editions. One, unedited, for Bible readers and their children; the other, expurgated, for persons of&nbsp;refinement. [M.T.] <a href="#ref-2">&#8617;</a></p>
	</article>
</footer>
