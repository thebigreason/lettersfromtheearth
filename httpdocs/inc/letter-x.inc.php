<h1>Letter X</h1>

<p>The two Testaments are interesting, each in its own way. The Old one gives us a picture of these people&rsquo;s Deity as he was before he got religion, the other one gives us a picture of him as he appeared afterward. The Old Testament is interested mainly in blood and sensuality. The New one in Salvation. Salvation by&nbsp;fire.</p>

<p>The first time the Deity came down to earth, he brought life and death; when he came the second time, he brought&nbsp;hell.</p>

<p>Life was not a valuable gift, but death was. Life was a fever-dream made up of joys embittered by sorrows, pleasure poisoned by pain, a dream that was a nightmare-confusion of spasmodic and fleeting delights, ecstasies, exultations, happinesses, interspersed with long-drawn miseries, griefs, perils, horrors, disappointments, defeats, humiliations, and despairs&mdash;the heaviest curse devisable by divine ingenuity; but death was sweet, death was gentle, death was kind; death healed the bruised spirit and the broken heart, and gave them rest and forgetfulness; death was man&rsquo;s best friend; when man could endure life no longer, death came and set him&nbsp;free.</p>

<p>In time, the Deity perceived that death was a mistake; a mistake, in that it was insufficient; insufficient, for the reason that while it was an admirable agent for the inflicting of misery upon the survivor, it allowed the dead person himself to escape from all further persecution in the blessed refuge of the grave. This was not satisfactory. A way must be conceived to pursue the dead beyond the&nbsp;tomb.</p>

<p>The Deity pondered this matter during four thousand years unsuccessfully, but as soon as he came down to earth and became a Christian his mind cleared and he knew what to do. He invented hell, and proclaimed&nbsp;it.</p>

<p>Now here is a curious thing. It is believed by everybody that while he was in heaven he was stern, hard, resentful, jealous, and cruel; but that when he came down to earth and assumed the name Jesus Christ, he became the opposite of what he was before: that is to say, he became sweet, and gentle, merciful, forgiving, and all harshness disappeared from his nature and a deep and yearning love for his poor human children took its place. Whereas it was as Jesus Christ that he devised hell and proclaimed&nbsp;it!</p>

<p>Which is to say, that as the meek and gentle Savior he was a thousand billion times crueler than ever he was in the Old Testament&mdash;oh, incomparably more atrocious than ever he was when he was at the very worst in those old&nbsp;days!</p>

<p>Meek and gentle? By and by we will examine this popular sarcasm by the light of the hell which he&nbsp;invented.</p>

<p>While it is true that the palm for malignity must be granted to Jesus, the inventor of hell, he was hard and ungentle enough for all godlike purposes even before he became a Christian. It does not appear that he ever stopped to reflect that <em>he</em> was to blame when a man went wrong, inasmuch as the man was merely acting in accordance with the disposition he had afflicted him with. No, he punished the man, instead of punishing himself. Moreover, the punishment usually oversized the offense. Often, too, it fell, not upon the doer of a misdeed, but upon somebody else&mdash;a chief man, the head of a community, for&nbsp;instance.</p>

<blockquote>
	<p>And Israel abode in Shittim, and the people began to commit whoredom with the daughters of&nbsp;Moab.</p>

	<p>And the Lord said unto Moses, Take <em>all the heads of the people</em>, and hang them up before the Lord against the Sun, that the fierce anger of the Lord may be turned away from&nbsp;Israel.</p>
</blockquote>

<p>Does that look fair to you? It does not appear that the &ldquo;heads of the people&rdquo; got any of the adultery, yet it is they that are hanged, instead of &ldquo;the&nbsp;people.&rdquo;</p>

<p>If it was fair and right in that day it would be fair and right today, for the pulpit maintains that God&rsquo;s justice is eternal and unchangeable; also that he is the Fountain of Morals, and that his morals are eternal and unchangeable. Very well, then, we must believe that if the people of New York should begin to commit whoredom with the daughters of New Jersey, it would be fair and right to set up a gallows in front of the city hall and hang the mayor and the sheriff and the judges and the archbishop on it, although they did not get any of it. It does not look right to&nbsp;me.</p>

<p>Moreover, you may be quite sure of one thing: it couldn&rsquo;t happen. These people would not allow it. They are better than their Bible. <em>Nothing</em> would happen here, except some lawsuits, for damages, if the incident couldn&rsquo;t be hushed up; and even down South they would not proceed against persons who did not get any of it; they would get a rope and hunt for the correspondents, and if they couldn&rsquo;t find them they would lynch a&nbsp;nigger.</p>

<p>Things have greatly improved since the Almighty&rsquo;s time, let the pulpit say what it&nbsp;may.</p>

<p>Will you examine the Deity&rsquo;s morals and disposition and conduct a little further? And will you remember that in the Sunday school the little children are urged to love the Almighty, and honor him, and praise him, and make him their model and try to be as like him as they can?&nbsp;Read:</p>

<blockquote>
	<p><sup>1</sup> And the Lord spake unto Moses,&nbsp;saying,</p>

	<p><sup>2</sup> Avenge the children of Israel of the Midianites: afterward shalt thou be gathered unto thy&nbsp;people&hellip;</p>

	<p><sup>7</sup> And they warred against the Midianites, as the Lord commanded Moses; and they slew all the&nbsp;males.</p>

	<p><sup>8</sup> And they slew the kings of Midian, beside the rest of them that were slain; <em>namely</em>, Evi, and Rekem, and Zur, and Hur, and Reba, five kings of Midian: Balaam also the son of Beor they slew with the&nbsp;sword.</p>

	<p><sup>9</sup> And the children of Israel took <em>all</em> the women of Midian captives, and their little ones, and took the spoil of all their cattle, and all their flocks, and all their&nbsp;goods.</p>

	<p><sup>10</sup> And they burnt all their cities wherein they dwelt, and all their goodly castles, with&nbsp;fire.</p>

	<p><sup>11</sup> And they took all the spoil, and all the prey, <em>both</em> of men and of&nbsp;beasts.</p>

	<p><sup>12</sup> And they brought the captives, and the prey, and the spoil unto Moses, and Eleazar the priest, and unto the congregation of the children of Israel, unto the camp at the plains of Moab, which <em>are</em> by Jordan <em>near</em>&nbsp;Jericho.</p>

	<p><sup>13</sup> And Moses, and Eleazar the priest, and all the princes of the congregation, went forth to meet them without the&nbsp;camp.</p>

	<p><sup>14</sup> And Moses was wroth with the officers of the host, <em>with</em> the captains over thousands, and captains over hundreds, which came from the&nbsp;battle.</p>

	<p><sup>15</sup> And Moses said unto them, Have ye saved all the women&nbsp;alive?</p>

	<p><sup>16</sup> Behold, these caused the children of Israel, through the counsel of Balaam, to commit trespass against the Lord in the matter of Peor, and there was a plague among the congregation of the&nbsp;Lord.</p>

	<p><sup>17</sup> Now therefore kill every male among the little ones, and kill every woman that hath known man by lying with&nbsp;him.</p>

	<p><sup>18</sup> But all the women children, that have not known a man by lying with him, keep alive for&nbsp;yourselves.</p>

	<p><sup>19</sup> And do ye abide without the camp seven days: whosoever hath killed any person, and whosoever hath touched any slain, purify <em>both</em> yourselves and your captives on the third day, and on the seventh&nbsp;day.</p>

	<p><sup>20</sup> And purify all <em>your</em> raiment, and all that is made of skins, and all work of goats&rsquo; <em>hair</em>, and all things made of&nbsp;wood.</p>

	<p><sup>21</sup> And Eleazar the priest said unto the men of war which went to the battle, This <em>is</em> the ordinance of the law which the Lord commanded&nbsp;Moses&hellip;</p>

	<p><sup>25</sup> And the Lord spake unto Moses,&nbsp;saying,</p>

	<p><sup>26</sup> Take the sum of the prey that was taken, <em>both</em> of man and of beast, thou, and Eleazar the priest, and the chief fathers of the&nbsp;congregation:</p>

	<p><sup>27</sup> And divide the prey into two parts; between them that took the war upon them, who went out to battle, and between all the&nbsp;congregation:</p>

	<p><sup>28</sup> And levy a tribute unto the Lord of the men of war which went out to&nbsp;battle&hellip;</p>

	<p><sup>31</sup> And Moses and Eleazar the priest did as the Lord commanded&nbsp;Moses.</p>

	<p><sup>32</sup> And the <em>booty</em>, being the rest of the prey which the men of war had caught, was six hundred thousand and seventy thousand and five thousand&nbsp;sheep,</p>

	<p><sup>33</sup> And threescore and twelve thousand&nbsp;beeves,</p>

	<p><sup>34</sup> And threescore and one thousand&nbsp;asses,</p>

	<p><sup>35</sup> And thirty and two thousand persons in all, of woman that had not known man by lying with him&hellip;</p>

	<p><sup>40</sup> And the persons <em>were</em> sixteen thousand; of which the Lord&rsquo;s tribute <em>was</em> thirty and two persons.</p>

	<p><sup>41</sup> And Moses gave the tribute, <em>which was</em> the Lord&rsquo;s heave offering, unto Eleazar the priest, as the Lord commanded Moses&hellip;</p>

	<p><sup>47</sup> Even of the children of Israel&rsquo;s half, Moses took one portion of fifty, <em>both</em> of man and of beast, and gave them unto the Levites, which kept the charge of the tabernacle of the Lord; as the Lord commanded&nbsp;Moses.</p>
</blockquote>

<blockquote>
	<p><sup>10</sup> When thou comest nigh unto a city to fight against it, then proclaim peace unto&nbsp;it&hellip;</p>

	<p><sup>13</sup> And when the Lord thy God hath delivered it into thine hands, thou shalt smite every male thereof with the edge of the&nbsp;sword:</p>

	<p><sup>14</sup> But the women, and the little ones, and the cattle, and all that is in the city, <em>even</em> all the spoil thereof, shalt thou take unto thyself; and thou shalt eat the spoil of thine enemies, which the Lord thy God hath given&nbsp;thee.</p>

	<p><sup>15</sup> Thus shalt thou do unto all the cities <em>which</em> are very far off from thee, which are not of the cities of these&nbsp;nations.</p>

	<p><sup>16</sup> But of the cities of these people, which the Lord thy God doth give thee for an inheritance, thou shalt save alive nothing that breatheth:</p>
</blockquote>

<p>The Biblical law says: &ldquo;Thou shalt not&nbsp;kill.&rdquo;</p>

<p>The law of God, planted in the heart of man at his birth, says: &ldquo;Thou shalt&nbsp;kill.&rdquo;</p>

<p>The chapter I have quoted shows you that the book-statute is once more a failure. It cannot set aside the more powerful law of&nbsp;nature.</p>

<p>According to the belief of these people, it was God himself who said: &ldquo;Thou shalt not&nbsp;kill.&rdquo;</p>

<p>Then it is plain that he cannot keep his own&nbsp;commandments.</p>

<p>He killed all those people&mdash;every&nbsp;male.</p>

<p>They had offended the Deity in some way. We know what the offense was, without looking; that is to say, we know it was a trifle; some small thing that no one but a god would attach any importance to. It is more than likely that a Midianite had been duplicating the conduct of one Onan, who was commanded to &ldquo;go into his brother&rsquo;s wife&rdquo;&mdash;which he did; but instead of finishing, &ldquo;he spilled it on the ground.&rdquo; The Lord slew Onan for that, for the lord could never abide indelicacy. The Lord slew Onan, and to this day the Christian world cannot understand why he stopped with Onan, instead of slaying all the inhabitants for three hundred miles around&mdash;they being innocent of offense, and therefore the very ones he would usually slay. For that had always been his idea of fair dealing. If he had had a motto, it would have read, &ldquo;Let no innocent person escape.&rdquo; You remember what he did in the time of the flood. There were multitudes and multitudes of tiny little children, and he knew they had never done him any harm; but their relations had, and that was enough for him: he saw the waters rise toward their screaming lips, he saw the wild terror in their eyes, he saw that agony of appeal in the mothers&rsquo; faces which would have touched any heart but his, but he was after the guiltless particularly, than he drowned those poor little&nbsp;chaps.</p>

<p>And you will remember that in the case of Adam&rsquo;s posterity <em>all</em> the billions are innocent&mdash;none of them had a share in his offense, but the Deity holds them guilty to this day. None gets off, except by acknowledging that guilt&mdash;no cheaper lie will&nbsp;answer.</p>

<p>Some Midianite must have repeated Onan&rsquo;s act, and brought that dire disaster upon his nation. If that was not the indelicacy that outraged the feelings of the Deity, then I know what it was: some Midianite had been pissing against the wall. I am sure of it, for that was an impropriety which the Source of all Etiquette never could stand. A person could piss against a tree, he could piss on his mother, he could piss on his own breeches, and get off, but he must not piss against the wall&mdash;that would be going quite too far. The origin of the divine prejudice against this humble crime is not stated; but we know that the prejudice was very strong&mdash;so strong that nothing but a wholesale massacre of the people inhabiting the region where the wall was defiled could satisfy the Deity.</p>

<p>Take the case of Jeroboam. &ldquo;I will cut off from Jeroboam him that pisseth against the wall.&rdquo; It was done. And not only was the man that did it cut off, but everybody&nbsp;else.</p>

<p>The same with the house of Baasha: everybody was exterminated, kinsfolks, friends, and all, leaving &ldquo;not one that pisseth against a wall.&rdquo;</p>

<p>In the case of Jeroboam you have a striking instance of the Deity&rsquo;s custom of not limiting his punishments to the guilty; the innocent are included. Even the &ldquo;remnant&rdquo; of that unhappy house was removed, even &ldquo;as a man taketh away dung, till it be all gone.&rdquo; That includes the women, the young maids, and the little girls. All innocent, for they couldn&rsquo;t piss against a wall. Nobody of that sex can. None but members of the other sex can achieve that&nbsp;feat.</p>

<p>A curious prejudice. And it still exists. Protestant parents still keep the Bible handy in the house, so that the children can study it, and one of the first things the little boys and girls learn is to be righteous and holy and not piss against the wall. They study those passages more than they study any others, except those which incite to masturbation. Those they hunt out and study in private. No Protestant child exists who does not masturbate. That art is the earliest accomplishment his religion confers upon him. Also the earliest her religion confers upon&nbsp;her.</p>

<p>The Bible has this advantage over all other books that teach refinement and good manners: that it goes to the child. It goes to the mind at its most impressible and receptive age&mdash;the others have to&nbsp;wait.</p>

<blockquote>
	<p>Thou shalt have a paddle upon thy weapon; and it shall be, when thou wilt ease thyself abroad, thou shalt dig therewith, and shalt turn back and cover that which cometh from&nbsp;thee.</p>
</blockquote>

<p>That rule was made in the old days because &ldquo;The Lord thy God walketh in the midst of thy&nbsp;camp.&rdquo;</p>

<p>It is probably not worthwhile to try to find out, for certain, why the Midianites were exterminated. We can only be sure that it was for no large offense; for the cases of Adam, and the Flood, and the defilers of the wall teach us that much. A Midianite may have left his paddle at home and thus brought on the trouble. However, it is no matter. The main thing is the trouble itself, and the morals of one kind and another that it offers for the instruction and elevation of the Christian of&nbsp;today.</p>

<p>God wrote upon the tables of stone: &ldquo;Thou shalt not kill,&rdquo; Also: &ldquo;Thou shalt not commit&nbsp;adultery.&rdquo;</p>

<p>Paul, speaking by the divine voice, advised against sexual intercourse <em>altogether</em>. A great change from the divine view as it existed at the time of the Midianite&nbsp;incident.</p>
