<h1>Preface</h1>
 
<p>In his will Mark Twain directed the Trustees of his Estate to conger with his daughter Clara and Albert B. Paine, his bigrapher, concerning the administration of his &ldquo;Literary productions.&rdquo; Paine served as literary executor from 1910, when Mark Twain died, until his own death in 1093. During this period he published his three-volume bigraphy, two volumes of letter, and a half-dozen other books drawn from the Mark Twain Papers. Shortly after Pain&rsquo;s death Harper &amp; Brothers recommended to the Trustees that the later Bernard DeVoto, author of <em>Mark Twain&rsquo;s America</em> (1932) and at that time editor of <em>The Saturday Review of Literature</em>, should be asked to take of the task of preparing for publication further material selected from the thousands of pages of unpublished writings by Mark Twain.</p>

<p>DeVoto recommended that three volumes should be brought out: (1) selections from the Autobiographical Dictation to supplement those edited by Paine; (2) a volume of letters; and (3) a collection of sketches and other short pieces. He began work at once on the third item, and in March, 1939, submitted to the Trustees the manuscript of <em>Letters from the Earth</em>, &ldquo;Ready for the printer.&rdquo; But when Clara Clemens read the manuscript she objected to the publication of certain parts of it on the ground that they presented a distorted view of er father&rsquo;s ideas and attitudes. The project was accordingly dropped, and the work has lain unpublished for more that twenty years in the successive depositories of the Mark Twain Papers, at Harvard, the Huntington Library, and the University of California, Berkeley.</p>

<p>During these two decades a long series of scholarly and critical studies&mdash;including DeVoto&rsquo;s own authoritative <em>Mark Twain at Work</em> (1942)&mdash;has demonstrated that the celebrated humorist was also a great artist. He now belongs beyond question among the major American writers. Since 1960, the fiftieth anniversary of Mark Twain&rsquo;s death, at least a dozen books about him have been published. In this abundance of knowledge and interpretation all his writings can be allowed to speak for themselves, and Clara Clemens has withdrawn her objections to the publication of <em>Letters from the Earth</em>. The books is now presented as DeVoto edited it in 1939, with only one or two minor changes in his editorial comments to take account of subsequent events.</p>

<p>The volume falls into two main parts: a first section, comprising &ldquo;Letters from the Earth,&rdquo; &ldquo;Papers of the Adam Family,&rdquo; and &ldquo;Letter to the Earth,&rdquo; that exhibits the astonishingly inventive play of Mark Twain&rsquo;s imagination about Biblical themes, or lunges out in satire of the world he lived in; and a more miscellaneous second section, containing DeVoto&rsquo;s selections from the remaining unpublished manuscripts in the Mark Twain Papers. These shorter pieces were written at intervals during four decades of Mark Twain&rsquo;s career and range in the mood from a whimsical take about cats he improvised for his children to the nightmare sea voyage of &ldquo;The Great Dark.&rdquo; Several of the have been published separately since 1939, as is indicated by the Bibliographical Note, but they are included here in order to preserve the integrity of DeVoto&rsquo;s editorial intention.</p>

<p>For some of the items in the collection DeVoto provided explanatory comments, which are printed in italics. He also wrote notes to be placed at the back of the book, mainly but not exclusively about problems of dating. His occasional footnotes are signed with his initials (&ldquo;B. DV.&rdquo;) to distinguish them from Mark Twain&rsquo;s footnotes, which are labeled &ldquo;M.T.&rdquo; It should be kept in mind that since Mark Twain did not prepare these manuscripts for the press, they contain minor errors and inconsistencies of form which DeVoto corrected in order to produce a readable text.</p>

<p>Mrs. Bernard DeVoto has kindly given her consent to the publication of what is in a double sense a posthumous work.</p>

<section class="byline">
	<address>
		<h4>Henry Nash Smith</h4>
		Literary Editor<br />of the Mark Twain Papers
	</address>
	<div>
		Berkeley<br />
		March, 1962
	</div>
</section>
