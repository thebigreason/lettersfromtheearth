# README #

Letters from the Earth is a late work by Mark Twain, written shortly before his death in 1910, and posthumously published in 1962. This codebase is an HTML edition meant to be an alternative to the inferior “canonical” version hosted at [Sacred Text Archive](http://www.sacred-texts.com/aor/twain/letearth.htm).

### Set Up ###

The publication is broken up into sections or chapters, and uses PHP includes to manage the page structure. A single bootstrap file that lives outside of the web root sets up a table of contents array which contains a slug and chapter title. This array is used for the navigation, index, and referencing the page includes. An include directory contains all the pages of the publication, including the index, chapters, and publisher’s notes with file names that correspond to the slugs in the table of contents array.

### Contribution Guidelines ###

Please feel free to submit corrections for typos, punctuation, typography, etc. Any punctuation, text formatting, or grammatical changes should match the first print edition of the book.

### Project Leader ###

@thebigreason 
mark@thebigreason.com